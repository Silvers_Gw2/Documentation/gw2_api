# Item Data
This is the main output of the api and where the majority of the customisation lies.

## Keys

```json
[
  "id",
  "name",
  "firstAdded",
  "lastHourlyUpdate",
  "lastStatsUpdate",
  "lastUpdate",
  "img",
  "type",
  "rarity",
  "chat_link",
  "default_skin",
  "weaponType",
  "..."
]
```

This returns a list of available parameters for both the json and csv sub-endpoints.

Example  
`https://api.silveress.ie/gw2/v1/items/keys`

Query:

* ~~Beautify~~
* ~~Fields~~
* ~~Filter~~
* ~~ID's~~
* ~~Start/End~~

## Json

````json
[
  {
    "id": 24,
    "lastUpdate": "2018-10-14T00:47:02.776Z",
    "buy_quantity": 45079,
    "buy_price": 161,
    "sell_quantity": 54061,
    "sell_price": 335,
    "name": "Sealed Package of Snowballs"
  },
  {
    "id": 68,
    "lastUpdate": "2018-10-14T00:47:02.776Z",
    "buy_quantity": 1504,
    "buy_price": 82,
    "sell_quantity": 369,
    "sell_price": 83,
    "name": "Mighty Country Coat"
  }
]
````

Outputs data on items in a flat array.  
Prices and quantities are updated every 3 min, Short term stats every 5 min,Item data and advanced stats are daily.  
Cache is 5 min.    
Default output is all marketable items.  
All items (marketable and not) can be requested by using ``filter=``


Example  
`https://api.silveress.ie/gw2/v1/items/json`

All queries apply.  
Query:

* Beautify
* Fields
* Filter
* ~~ID's~~
* ~~Start/End~~


## CSV

````CSV
"id","name","lastUpdate","buy_quantity","buy_price","sell_quantity","sell_price"
24,"Sealed Package of Snowballs","2018-10-14T00:56:01.840Z",45079,161,54061,335
68,"Mighty Country Coat","2018-10-14T00:56:01.840Z",1503,82,369,83
69,"Mighty Country Coat","2018-10-14T00:56:01.840Z",1971,94,704,176
````

Outputs data in a format suitable for spreadsheets.  
Prices and quantities are updated every 3 min, Short term stats every 5 min,Item data and advanced stats are daily.  
Cache is 5 min.    
Default output is all marketable items.  
All items (marketable and not) can be requested by using ``filter=``

Example  
`https://api.silveress.ie/gw2/v1/items/csv`

Most queries apply.  
Query:

* ~~Beautify~~
* Fields
* Filter
* ~~ID's~~
* ~~Start/End~~
