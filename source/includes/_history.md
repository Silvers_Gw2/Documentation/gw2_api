# History
History of an item, compressed into daily segments.  
Only available as JSON

## History

```json
[
  {
    "id": 19721,
    "date": "2018-07-27T00:00:00.000Z",
    "count": 277,
    "buy_quantity_total": 51932098,
    "buy_price_total": 614129,
    "sell_quantity_total": 236063312,
    "sell_price_total": 665888,
    "buy_quantity_max": 198484,
    "buy_price_max": 2444,
    "sell_quantity_max": 893462,
    "sell_price_max": 2487,
    "buy_quantity_min": 179018,
    "buy_price_min": 2143,
    "sell_quantity_min": 821095,
    "sell_price_min": 2165,
    "sell_velocity": 217895,
    "buy_velocity": 105766,
    "sell_value": 524730476,
    "buy_value": 234557057
  }
]
```

Returns the history for the selected Items

Example  
`https://api.silveress.ie/gw2/v1/history?ids=19721`  
`https://api.silveress.ie/gw2/v1/history?ids=19721,19976&beautify=min&start=2018-10-01&end=2018-10-25T00:40:28%2B01:00`

Query:

* Beautify
* ~~Fields~~
* ~~Filter~~
* ID's
* Start/End
