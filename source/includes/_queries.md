# Queries

This is the secret sauce of my api, queries that allow a large degree of control over what is returned.
These queries are used to modify the output of the api in order to give the user the greatest control over the results.  

### Queries
Queries are done in the format of:  
``?query1=query1Parameter&query2=query2Parameter&..&queryN=queryNParameter``

### Parameters
Parameters are comma separated strings:  
``?query1=parm1,parm2,parm3,...,parmN``

### Sub Parameters
Sub parameters are further division of parms.  
They take the form of:  
``parm1:modifier``

### Boolean  
To pass boolean arguments they are to be sent in all caps.  
``filter=AccountBound:TRUE``

## Beautify

This controls the format of outputted JSON.  
Default is ``human``  
Other option is ``min``

Example  
`https://api.silveress.ie/gw2/v1/items/json?beautify=human`  
`https://api.silveress.ie/gw2/v1/items/json?beautify=min`

## Fields

This is used to modify what datafields are returned.  
Parameters are the same as ``/keys`` endpoints

Example  
``https://api.silveress.ie/gw2/v1/items/json?fields=id,name,default_skin``  


## Filter

Filter is probably the most complex query, utilising sub parameters such as the aforementioned ``filter=AccountBound:TRUE``.

### Quantifiers
Using Sub Parameters it is possible to use operators to request specific information.  
They use the style of [MongoDB Operators](https://docs.mongodb.com/manual/reference/operator/query-comparison/) 

Examples:  
``https://api.silveress.ie/gw2/v1/items/json?filter=id:gte:500``  
``https://api.silveress.ie/gw2/v1/items/json?filter=id:gte:500,buy_quantity:lt:1000``  
``https://api.silveress.ie/gw2/v1/items/json?filter=id:gte:500,buy_quantity:lt:1000,year_sell_quantity_avg:gte:2000``  


## ID's

This allows sending multiple IDs to batch the results into one request.  
Its a comma separated list.  
``https://api.silveress.ie/gw2/v1/history?ids=19721,19976``


## Start/End

By passing a date in ISO 8601 standard the results can be specified from a particular date period.  
``https://api.silveress.ie/gw2/v1/history?ids=19721&start=2018-10-01&end=2018-10-05``
``https://api.silveress.ie/gw2/v1/history?ids=19721&start=2018-10-01&end=2018-10-25T00:40:28``

The offset must be properly url encoded:  
``https://api.silveress.ie/gw2/v1/history?ids=19721&start=2018-10-01&end=2018-10-25T00:40:28%2B01:00``  
Not   
``https://api.silveress.ie/gw2/v1/history?ids=19721&start=2018-10-01&end=2018-10-25T00:40:28+01:00``
