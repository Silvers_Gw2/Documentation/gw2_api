# Transactions

This is the section that manages getting, compressing, archiving and delivering the results.

## Process

So how does it work?  
1. So when you submit your details (API key, an email and a password) it then creates and account and adds it to the queue.  
2. Within a minute my server starts pulling your history.
    * While doing this it takes note of what is the latest transaction and records that.
    * This process can take a few minutes for a small history up to 45 min if you are a power trader.
3. It compresses the transactions down into day snapshots.  
3. The next time the process runs it looks for items newer than the last snapshot

(Shamelessly copied from the site documentation)

## Accessing data

To access the data you can call ``https://api.silveress.ie/gw2/v1/transactions/{buy/sell}`` and use headers of ``apiKey`` and ``password``.
The api key will be resolved into its account guid so it requires teh ``account`` permission.
The database is then searched to see if that guid and password combination exist, if it does not it returns a 401 result.

The amount of data the request delivers depends on the account level.

* 3
   - Returns everything.
* 2
   - Returns 12 months worth of data.
* 1
   - Returns 6 months worth of data.
   
## Patreon

The patreon script runs hourly and matches the emails of patrons to the email on the account in my db.

