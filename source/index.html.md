---
title: Silveress's Gw2 API Reference

language_tabs: # must be one of https://git.io/vQNgJ

toc_footers:

 #  - <a href='#'>Sign Up for a Developer Key</a>
  - <a href='https://github.com/lord/slate'>Documentation Powered by Slate</a>

includes:
  - queries
  - items
  - history
  - transactions

search: true
---

# Introduction


Welcome to my documentation on my Gw2 API, finally got around to writing it.

Headers are on the left, more detailed info on the endpoints in the center and the outputs on the right.

This example API documentation page was created with [Slate](https://github.com/lord/slate).
